#language: es

@HistoriaUsuario
Característica: Verificar el ingreso al aplicativo X
  yo como usuario quiero ingresar a la app x para validar que puedo acceder de manera correcta
  @caso1
  Escenario: Verificar el acceso correcto a la plataforma
    Dado que Andres ingresa a la pagina X
    Cuando ingresa sus credenciales
    Entonces Verifica que pudo acceder de manera correcta a la app