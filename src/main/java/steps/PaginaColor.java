package steps;

import net.thucydides.core.annotations.Step;
import pageobject.PaginaUrlColor;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.*;

public class PaginaColor {
    private PaginaUrlColor paginaUrlColor;

    @Step
    public void appColor(){
        paginaUrlColor.open();
    }

    @Step
    public void acciones(){
        paginaUrlColor.usuario.sendKeys("AndresPom");
        paginaUrlColor.contrasenna.sendKeys("123456789123456789");
        paginaUrlColor.btn_sing_in.click();
    }

    @Step
    public void verificar(){
        assertThat(paginaUrlColor.verificacion.isCurrentlyVisible(), is(true));
    }
}
