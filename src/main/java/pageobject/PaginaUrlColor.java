package pageobject;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class PaginaUrlColor extends PageObject {

    @FindBy(xpath = "//input[@placeholder='Username']")
    public WebElementFacade usuario;
    @FindBy(xpath = "//input[@placeholder='Password']")
    public WebElementFacade contrasenna;
    @FindBy(css = "#login > form > button")
    public WebElementFacade btn_sing_in;
    @FindBy(className = "nav-header")
    public WebElementFacade verificacion;
}
