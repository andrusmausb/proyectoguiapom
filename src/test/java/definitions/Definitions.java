package definitions;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import steps.PaginaColor;

public class Definitions {

    @Steps
    PaginaColor paginaColor;

    @Dado("^que Andres ingresa a la pagina X$")
    public void queAndresIngresaALaPaginaX() {
        paginaColor.appColor();
    }

    @Cuando("^ingresa sus credenciales$")
    public void ingresaSusCredenciales() {
        paginaColor.acciones();
    }

    @Entonces("^Verifica que pudo acceder de manera correcta a la app$")
    public void verificaQuePudoAccederDeManeraCorrectaALaApp() {
        paginaColor.verificar();
    }
}
